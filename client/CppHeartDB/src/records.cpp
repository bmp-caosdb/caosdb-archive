/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#include "headers/records.h"

list<recordType*> recordType::fromXml(string stream){
	list<recordType*> recordList;
	stringstream ss;
	ss.str (stream);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	concreteProperty* cProp;
	recordType* rt;
	parent* par;
	for (pugi::xml_node node = doc.child("Response").first_child(); node; node = node.next_sibling()){			
		if(error::errorCheck(node)){		
			rt = new record();
			string id= node.attribute("id").value();
			rt->setId(atoi(id.c_str()));
			rt->setName(node.attribute("name").value());
			rt->setDescription(node.attribute("description").value());
			for (pugi::xml_node nodeP = node.first_child(); nodeP; nodeP = nodeP.next_sibling()){
				string name=nodeP.name();
				if(!name.compare("Property")){
					cProp=new concreteProperty();
					string id= nodeP.attribute("id").value();
					cProp->setId(atoi(id.c_str()));
					cProp->setName(nodeP.attribute("name").value());
					cProp->setType(nodeP.attribute("type").value());
					cProp->setDescription(nodeP.attribute("description").value());
					cProp->setGenerator(nodeP.attribute("generator").value());
					cProp->setUnit(nodeP.attribute("unit").value());
					string reference= nodeP.attribute("reference").value();
					cProp->setReference(atoi(reference.c_str()));
					cProp->setCreator(nodeP.attribute("creator").value());
					cProp->setCreated(nodeP.attribute("created").value());
					cProp->setImportance(nodeP.attribute("importance").value());
					string exponent= nodeP.attribute("exponent").value();
					cProp->setReference(atoi(exponent.c_str()));
					cProp->setValue(nodeP.child_value());
					rt->setProperty(cProp);
				}
				else if(!name.compare("Parent")){
					par = new parent();
					string id= nodeP.attribute("id").value();
					par->setId(atoi(id.c_str()));
					par->setName(node.attribute("name").value());
					par->setDescription(node.attribute("description").value());
					rt->setParent(par);
				}
			}
			recordList.push_back(rt);
		}
	}
	return recordList;
}

string recordType::toXml(list<recordType*> recordTypeList, string type){
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("Post");
	for (list<recordType*>::iterator rt = recordTypeList.begin(), end = recordTypeList.end(); rt != end; ++rt)
	{	
		pugi::xml_node rtNode = root.append_child(type.c_str());
		if((*rt)->getId()!=0)
			rtNode.append_attribute("id") = (*rt)->getId();
        	if((*rt)->getName() !="")
			rtNode.append_attribute("name") = (*rt)->getName().c_str();
		if((*rt)->getDescription() !="")
			rtNode.append_attribute("description") = (*rt)->getDescription().c_str();
		for (list<parent*>::iterator par = (*rt)->getParents().begin(), end = (*rt)->getParents().end(); par != end; ++par){
			pugi::xml_node par2 = rtNode.append_child("Parent");
			if((*par)->getId()!=0)
				par2.append_attribute("id") = (*par)->getId();
			if((*par)->getTransfer()!="")
				par2.append_attribute("transfer") = (*par)->getTransfer().c_str();
		}
		for (list<concreteProperty*>::iterator cProp = (*rt)->getProperties().begin(), end = (*rt)->getProperties().end(); cProp != end; ++cProp){
			pugi::xml_node property = rtNode.append_child("Property");
			if((*cProp)->getId()!=0)
				property.append_attribute("id") = (*cProp)->getId();
			if((*cProp)->getName()!="")
				property.append_attribute("name") = (*cProp)->getName().c_str();
			if((*cProp)->getImportance()!="")
				property.append_attribute("importance") = (*cProp)->getImportance().c_str();
			if((*cProp)->getValue()!="")
				property.append_child(pugi::node_pcdata).set_value((*cProp)->getValue().c_str());
		}
    	}	
	stringstream stream;  
	doc.print(stream);
	return stream.str();
}

void recordType::setFromResponse(list<recordType*> recordTypeList, string xmlResponse){
	stringstream ss;
	ss.str (xmlResponse);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	list<concreteProperty*> cPropList; 
	list<recordType*>::iterator rt = recordTypeList.begin(), end = recordTypeList.end(); 
	for (pugi::xml_node node = doc.child("Response").first_child(); node && rt != end; node = node.next_sibling(), ++rt){
		if(error::errorCheck(node)){		
			string id= node.attribute("id").value();
			(*rt)->setId(atoi(id.c_str()));
			cout << "File successfully inserted. ID: " << (*rt)->getId() << endl;
		}
	}
}
