/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#include "error.h"

bool error::errorCheck(pugi::xml_node node){
	bool flag=true;
	for (pugi::xml_node nodeP = node.first_child(); nodeP; nodeP = nodeP.next_sibling()){
		string name=nodeP.name();
		if(!name.compare("Error")){
			cout << "Error "<< " id: "<< node.attribute("id").value() <<" name: " << node.attribute("name").value() <<"  code: " << nodeP.attribute("code").value() 
			<< "   " << nodeP.attribute("description").value() << endl;
			flag=false;		
		}
		else if(!name.compare("Warning")){
			cout << "Warning "<< " id: "<< node.attribute("id").value() <<" name: " << node.attribute("name").value() <<"  code: " << nodeP.attribute("code").value() 
			<< "   " << nodeP.attribute("description").value() << endl;
		}
	}
	return flag;
}

void error::errorCheck(string stream){
	stringstream ss;
	ss.str (stream);
	pugi::xml_document doc;
	pugi::xml_parse_result xmlres = doc.load(ss);
	pugi::xml_node node = doc.child("Response").first_child();
	if(errorCheck(node)){
		string name=node.first_child().name();
		if(!name.compare("Info")){
			if(node.first_child().name()){
				cout << "ID/name: " << node.attribute("id").value() <<"  name: " << node.attribute("name").value() << "  description: " <<  
				node.first_child().attribute("description").value() << endl;
			}
		}
		else
			cout << "Entity successfully updated " << endl;
	}
}
