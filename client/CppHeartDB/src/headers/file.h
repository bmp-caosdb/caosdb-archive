/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* 
 * File:   file.h
 * Author: chris-andre
 */

#ifndef FILE_H
#define	FILE_H

#include <tr1/memory>
#include <string>
#include "pugixml.hpp"
#include <list>
#include <sstream>
#include <iostream>
#include <stdlib.h> 
#include <iterator>
#include <openssl/sha.h>
#include <vector>
#include <stdio.h>
#include <iterator>
#include "error.h"

#define HTTP_UPLOAD 0
#define DROP_OFF_BOX 1
#define HTTP_DOWNLOAD 2
#define FILE_SYSTEM 3

using namespace std;

class file
{
	public:
	file(){
		this->size=0;
		this->id=0;
		this->cuid=0;
	}
	file(int id){
		this->size=0;
		this->id=id;
		this->cuid=0;
	}
	
	virtual ~file(){};
	
	file& operator* ()  { 
		tr1::shared_ptr<file>  filePtr;
		return *filePtr; 
	}
  
	//Getter
	int getId(){return id;}
	int getSize(){return size;}
	string getUpload(){return upload;}
	string getName(){return name;}
	string getPath(){return path;}
	string getDescription(){return description;}
	string getChecksum(){return checksum;}
	string getPickup(){return pickup;}
	int getCuid(){return cuid;}
	char* getFile(){return reinterpret_cast<char*> (&this->fileBuffer[0]);}
	
	//Setter
	void setId(int id){this->id=id;}
	void setCuid(int cuid){this->cuid=cuid;}
	void setSize(int size){this->size=size;}
	void setName(string name){this->name=name;}
	void setPickup(string pickup){this->pickup=pickup;}
	void setUpload(string upload){this->upload=upload;}
	void setPath(string path){this->path=path;}
	void setDescription(string description){this->description=description;}
	void setFile(string name);
	void setChecksum(string checksum){this->checksum=checksum;}
	
	//XML Parse and create functions
	static string toXml(list<file*> fileList);
 	static void setFromResponse(list<file*>& fileList, string xmlResponse);
	static list<file*> fromXml(string stream);
	vector<char> fileBuffer;
	
	//Object method for checksum check
	bool checkFile();

	private:
	string upload;
	string name;
	string path;
	string description;
	string checksum;
	string pickup;
	unsigned long long size;
	int id;
	int cuid;
};

#endif	
