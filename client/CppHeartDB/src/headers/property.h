/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* 
 * File:   property.h
 * Author: chris-andre
 */

#ifndef PROPERTY_H
#define	PROPERTY_H

#include <tr1/memory>
#include <string>
#include "pugixml.hpp"
#include "error.h"
#include <list>
#include <sstream>
#include <iostream>
#include <stdlib.h> 
#include <iterator>


using namespace std;

class abstractProperty
{
	public:
	
	abstractProperty(){
		this->id=0;
		this->reference=0;
	}
	abstractProperty(int id){
		this->id=id;
		this->reference=0;
	}

	abstractProperty(string name){
		this->id=0;
		this->name=name;
		this->reference=0;
	}
	
	virtual ~abstractProperty(){};
	
	abstractProperty& operator* ()  { 
		tr1::shared_ptr<abstractProperty>  abstractPropertyPtr;
		return *abstractPropertyPtr; 
	}
  
	//Getter
	int getId(){return id;}
	int getReference(){return reference;}
	string getName(){return name;}
	string getType(){return type;}
	string getDescription(){return description;}
	string getUnit(){return unit;}
	string getGenerator(){return generator;}
	string getCreator(){return creator;}
	string getCreated(){return created;}
  
	//Setter
	void setId(int id){this->id=id;}
	void setReference(int reference){this->reference=reference;}
	void setName(string name){this->name=name;}
	void setType(string type){this->type=type;}
	void setDescription(string description){this->description=description;}
	void setUnit(string unit){this->unit=unit;}
	void setGenerator(string generator){this->generator=generator;}
	void setCreator(string creator){this->creator=creator;}
	void setCreated(string created){this->created=created;}
	
	//XML Parse and create functions
	static list<abstractProperty*> fromXml(string);
	static string toXml(list<abstractProperty*>);
	static void setIdFromResponse(list<abstractProperty*> aPropList, string xmlResponse);
  
	private:
	string name;
	string type;
	string description;
	string unit;
	string generator;
	string creator;
	string created;
	int reference;
	int id;
	int cuid;
};

class concreteProperty : public abstractProperty
{
	public:
	concreteProperty():abstractProperty(){
		this->exponent=0;
	}
	concreteProperty(int id):abstractProperty(id){
		this->exponent=0;
	}
	concreteProperty(string name):abstractProperty(name){
		this->exponent=0;
	}
	
	virtual ~concreteProperty(){};
	
	concreteProperty& operator* ()  { 
		tr1::shared_ptr<concreteProperty>  concretePropertyPtr;
		return *concretePropertyPtr; 
	}
  
	//Getter
	string getImportance(){return importance;}
	string getValue(){return value;}
	int getExponent(){return exponent;}
	
	//Setter
	void setImportance(string importance){this->importance=importance;}
	void setValue(string value){this->value=value;}
	void setExponent(int exponent){this->exponent=exponent;}
  
	private:
	string importance;
	int exponent;
	string value;
};

#endif	
