/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#ifndef RECORDS_H
#define	RECORDS_H

#include <string>
#include <list>
#include <tr1/memory>
#include <iostream>
#include "property.h"
#include "error.h"
#include "pugixml.hpp"

using namespace std;

class baseRecord
{
public:
	baseRecord(){this->id=0;}

	baseRecord(string name){
		this->name=name;
		this->id=0;}

	baseRecord(string name, string description){
		this->name=name;
		this->description=description;
		this->id=0;}

	baseRecord(int id){
		this->id=id;}

	baseRecord(int id, string name){
		this->name=name;
		this->id=id;}

	baseRecord(int id, string name, string description){
		this->name=name;
		this->id=id;
		this->description=description;}

	baseRecord& operator*(){ 
		tr1::shared_ptr<baseRecord> baseRecordPtr;
	return *baseRecordPtr; }	

	string getName(){return name;}
	string getDescription(){return description;}
	int getId(){return id;}
	int getCuid(){return cuid;}

	void setName(string name){this->name=name;}
	void setDescription(string description){this->description=description;}
	void setId(int id){this->id=id;}
	void setCuid(int cuid){this->cuid=cuid;}

private:
	int id;
	int cuid;
	string name;
	string description;
};

class parent : public baseRecord
{
public:
	parent():baseRecord(){}
	parent(string name):baseRecord(name){}
	parent(string name, string description):baseRecord(name, description){}
	parent(int id):baseRecord(id){}
	parent(int id, string name):baseRecord(id, name){}
	parent(int id, string name, string description):baseRecord(id, name, description){}
	parent(int id, int transfer):baseRecord(id){this->transfer=transfer;}
	parent(string name, int transfer):baseRecord(name){this->transfer=transfer;}

	parent& operator*(){ 
		tr1::shared_ptr<parent> parentPtr;
	return *parentPtr;}
	
	string getTransfer(){return transfer;}	
	
	void setTransfer(string transfer){this->transfer=transfer;}

private:
	string transfer;
};
class recordType : public baseRecord
{
public:
	recordType():baseRecord(){}
	recordType(string name):baseRecord(name){}
	recordType(string name, string description):baseRecord(name, description){}
	recordType(int id):baseRecord(id){}
	recordType(int id, string name):baseRecord(id, name){}
	recordType(int id, string name, string description):baseRecord(id, name, description){}
	recordType(string name, list<concreteProperty*>& propList):baseRecord(name){this->properties.insert(this->properties.end(), propList.begin(), propList.end());}
	recordType(string name,string description, list<concreteProperty*>& propList):baseRecord(name, description){this->properties.insert(this->properties.end(), propList.begin(), propList.end());}
	recordType(int id, list<concreteProperty*>& propList):baseRecord(id){this->properties.insert(this->properties.end(), propList.begin(), propList.end());}
	recordType(int id, string name, list<concreteProperty*>& propList):baseRecord(id, name){this->properties.insert(this->properties.end(), propList.begin(), propList.end());}
	recordType(int id, string name, string description, list<concreteProperty*>& propList):baseRecord(id, name, description){this->properties.insert(this->properties.end(), propList.begin(), propList.end());}
	
	recordType& operator* (){ 
		tr1::shared_ptr<recordType> recordTypePtr;
	return *recordTypePtr;}

	list<concreteProperty*>& getProperties(){return properties;}
	list<parent*>& getParents(){return parents;}

	void setProperties(list<concreteProperty*>& propList){
		this->properties.insert( this->properties.end(), propList.begin(), propList.end() );}
	void setProperty(concreteProperty* prop){this->properties.push_back(prop);}
	void setParents(list<parent*>& parList){
		this->parents.insert( this->parents.end(), parList.begin(), parList.end() );}
	void setParent(parent* par){this->parents.push_back(par);}
	
	static list<recordType*> fromXml(string);
	static string toXml(list<recordType*>, string type);
	static void setFromResponse(list<recordType*> recordTypeList, string xmlResponse);

private:
	list<concreteProperty*> properties;
	list<parent*> parents;
};


class record :  public recordType
{
public:
	record():recordType(){}
	record(string name):recordType(name){}
	record(string name, string description):recordType(name, description){}
	record(int id):recordType(id){}
	record(int id, string name):recordType(id, name){}
	record(int id, string name, string description):recordType(id, name, description){}
	record(string name, list<concreteProperty*>& propList):recordType(name, propList){}
	record(string name, string description, list<concreteProperty*>& propList):recordType(name, description, propList){}
	record(int id, list<concreteProperty*>& propList):recordType(id, propList){}
	record(int id, string name, list<concreteProperty*>& propList):recordType(id, name, propList){}
	record(int id, string name, string description, list<concreteProperty*>& propList):recordType(id, name, description, propList){}

	record& operator*(){ 
		tr1::shared_ptr<record> recordPtr;
	return *recordPtr;}

	static string toXml(list<record*>);
	static void setFromResponse(list<record*> recordList, string xmlResponse);
};












#endif	
