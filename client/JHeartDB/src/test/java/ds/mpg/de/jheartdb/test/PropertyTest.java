/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.structures.PropertyType.DATETIME;
import static ds.mpg.de.jheartdb.structures.PropertyType.DOUBLE;
import static ds.mpg.de.jheartdb.structures.PropertyType.FILE;
import static ds.mpg.de.jheartdb.structures.PropertyType.INTEGER;
import static ds.mpg.de.jheartdb.structures.PropertyType.REFERENCE;
import static ds.mpg.de.jheartdb.structures.PropertyType.TEXT;
import static ds.mpg.de.jheartdb.structures.PropertyType.TIMESPAN;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRand;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.PropertyType;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * @author Timm Fitschen
 * 
 */
public class PropertyTest {

    @Test
    public void testInsertSingleProperty() {

        // Insert good integer property
        Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Integer");
        assertNotNull(p.getType());
        assertEquals("Integer".toUpperCase(), p.getType().toString().toUpperCase());
        p.setUnit("m");
        LinkedList<Property> ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());

        // Insert good double property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Double");
        p.setUnit("m");
        ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(DOUBLE, ret.getFirst().getType());

        // Insert good text property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Text");
        ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(TEXT, ret.getFirst().getType());

        // Insert good datetime property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Datetime");
        ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(DATETIME, ret.getFirst().getType());

        // Insert good timespan property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Timespan");
        p.setUnit("h");
        ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("h", ret.getFirst().getUnit());
        assertEquals(TIMESPAN, ret.getFirst().getType());

        // Insert good file property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "File");
        ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(FILE, ret.getFirst().getType());

        // Insert good reference property
        p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest", "Reference");
        final LinkedList<RecordType> refTypes = getHeartDBClient().retrieveRecordType("all");
        if (refTypes != null) {
            assertNotNull(refTypes.getFirst());
            final Integer refid = refTypes.get(getRand().nextInt(refTypes.size())).getId();
            p.setReferenceTypeID(refid);
            ret = getHeartDBClient().insertProperty(p);
            assertNotNull(ret);
            assertEquals(1, ret.size());
            assertNotNull(ret.getFirst().getMessages());
            assertEquals(0, ret.getFirst().getMessages().size());
            assertNotNull(ret.getFirst().getId());
            assertNotNull(ret.getFirst().getName());
            assertNotNull(ret.getFirst().getType());
            assertNotNull(ret.getFirst().getDescription());
            assertEquals(refid, ret.getFirst().getReferenceTypeID());
            assertEquals(REFERENCE, ret.getFirst().getType());
        }
    }

    @Test
    public void updateProperty() {

        // Insert good integer property
        final Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Integer");
        p.setUnit("m");

        // insert
        LinkedList<Property> ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        final Integer id = ret.getFirst().getId();

        // update name
        final String name = "HeartDBClientTest" + getRandString() + "update";
        for (final Property pupdate : ret) {
            pupdate.setName(name);
        }

        ret = getHeartDBClient().updateProperty(ret);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());

        // update description
        final String desc = "HeartDBClientTest" + "update";
        for (final Property pupdate : ret) {
            pupdate.setDescription(desc);
        }

        ret = getHeartDBClient().updateProperty(ret);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(ret.getFirst().getDescription(), desc);
        assertNotNull(ret.getFirst().getUnit());
        assertEquals("m", ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());

        // update unit
        final String unit = "updatem";
        for (final Property pupdate : ret) {
            pupdate.setUnit(unit);
        }

        ret = getHeartDBClient().updateProperty(ret);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(ret.getFirst().getDescription(), desc);
        assertNotNull(ret.getFirst().getUnit());
        assertEquals(unit, ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());

        // update exponent
        final Integer exp = 10;
        for (final Property pupdate : ret) {
            pupdate.setExponent(exp);
        }

        ret = getHeartDBClient().updateProperty(ret);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(ret.getFirst().getDescription(), desc);
        assertNotNull(ret.getFirst().getUnit());
        assertEquals(unit, ret.getFirst().getUnit());
        assertEquals(INTEGER, ret.getFirst().getType());
        assertNotNull(ret.getFirst().getExponent());
        assertEquals(exp, ret.getFirst().getExponent());

        // update type & refid
        final PropertyType type = REFERENCE;
        final Integer refid = 1;
        for (final Property pupdate : ret) {
            pupdate.setType(type);
            pupdate.setExponent((Integer) null);
            pupdate.setUnit(null);
            pupdate.setReferenceTypeID(refid);
        }

        ret = getHeartDBClient().updateProperty(ret);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(ret.getFirst().getDescription(), desc);
        assertNull(ret.getFirst().getUnit());
        assertEquals(type, ret.getFirst().getType());
        assertNull(ret.getFirst().getExponent());
        assertNotNull(ret.getFirst().getReferenceTypeID());
        assertEquals(refid, ret.getFirst().getReferenceTypeID());

        // retrieve everything again to test whether it was actually written.
        ret = getHeartDBClient().retrieveProperty(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertNotNull(ret.getFirst().getName());
        assertEquals(ret.getFirst().getName(), name);
        assertNotNull(ret.getFirst().getType());
        assertNotNull(ret.getFirst().getDescription());
        assertEquals(ret.getFirst().getDescription(), desc);
        assertNull(ret.getFirst().getUnit());
        assertEquals(type, ret.getFirst().getType());
        assertNull(ret.getFirst().getExponent());
        assertNotNull(ret.getFirst().getReferenceTypeID());
        assertEquals(refid, ret.getFirst().getReferenceTypeID());

    }

    @Test
    public void deleteProperty() {
        // Insert good integer property
        final Property p = new Property("HeartDBClientTest" + getRandString(), "HeartDBClientTest",
                "Integer");
        p.setUnit("m");
        LinkedList<Property> ret = getHeartDBClient().insertProperty(p);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        final Integer id = ret.getFirst().getId();

        // test for correct deletion message.
        ret = getHeartDBClient().deleteProperty(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertTrue(ret.getFirst().getMessages().getFirst().getType().equalsIgnoreCase("Info"));
        assertEquals(ret.getFirst().getMessages().getFirst().getDescription(),
                "This entity has been deleted.");

        // retrieve everything again to test whether it was actually deleted.
        ret = getHeartDBClient().retrieveProperty(id);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertTrue(ret.getFirst().getMessages().getFirst().getType().equalsIgnoreCase("Error"));
        assertEquals(ret.getFirst().getMessages().getFirst().getCode(), (Integer) 101);
    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }
}
