/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * @author Timm Fitschen
 * 
 */
public class CleanUp {

    @Test
    public void deleteAllGeneratedPropertiesAndOtherEntities() {
        final Scanner input = new Scanner(System.in);
        System.out.println("delete all generated entities [y/n]?");
        if (input.next().equals("y")) {
            System.out.println("retrieving recordtypes...");
            final LinkedList<RecordType> recordTypes = getHeartDBClient().retrieveRecordType("all");
            System.out.println("retrieving records...");
            final LinkedList<Record> records = getHeartDBClient().retrieveRecord("all");
            System.out.println("retrieving properties...");
            final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
            System.out.println("retrieving files...");
            final LinkedList<File> files = getHeartDBClient().retrieveFile("all");
            final ArrayList<Integer> ids = new ArrayList<Integer>();
            if (recordTypes != null) {
                for (final RecordType rt : recordTypes) {
                    if (rt.getName().startsWith("HeartDBClientTest")) {
                        ids.add(rt.getId());
                    }
                }
            }
            if (records != null) {
                for (final Record rec : records) {
                    if (rec.getName().startsWith("HeartDBClientTest")) {
                        ids.add(rec.getId());
                    }
                }
            }
            if (properties != null) {
                for (final Property p : properties) {
                    if (p.getName().startsWith("HeartDBClientTest")) {
                        ids.add(p.getId());
                    }
                }
            }
            if (files != null) {
                for (final File f : files) {
                    if (f.getName().startsWith("HeartDBClientTest")) {
                        ids.add(f.getId());
                    }
                }
            }
            final LinkedList<Property> ret = getHeartDBClient().deleteProperty(ids);
            if (!ids.isEmpty()) {
                assertNotNull(ret);
                assertEquals(ids.size(), ret.size());
            }

            final LinkedList<Property> ps = getHeartDBClient().retrieveProperty("all");
            if (ps != null) {
                for (final Property p : ps) {
                    assertFalse(p.getName().startsWith("HeartDBClientTest"));
                }
            }
            final LinkedList<Record> recs = getHeartDBClient().retrieveRecord("all");
            if (recs != null) {
                for (final Record rec : recs) {
                    assertFalse(rec.getName().startsWith("HeartDBClientTest"));
                }
            }
            final LinkedList<RecordType> rts = getHeartDBClient().retrieveRecordType("all");
            if (rts != null) {
                for (final RecordType rt : rts) {
                    assertFalse(rt.getName().startsWith("HeartDBClientTest"));
                }
            }
        }
    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }
}
