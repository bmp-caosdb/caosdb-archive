/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/**
 * 
 */
package ds.mpg.de.jheartdb.test;

import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getHeartDBClient;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRand;
import static ds.mpg.de.jheartdb.test.HeartDBClientTest.getRandString;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

import java.io.IOException;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Test;

import ds.mpg.de.jheartdb.structures.Message;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;

/**
 * @author Timm Fitschen
 * 
 */
public class RecordTypeTest {
    @Test
    public void insertSingleRecordType() {

        // insert good recordType
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final RecordType rt = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        int i = 0;
        for (final Property p : properties) {
            if (p.getId() >= 100) {
                i++;
            }
        }
        if (i < 3) {
            return;
        }
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));

            if (p.getId() < 100) {
                continue;
            } else {
                rt.addProperty(p, "obligatory");
                i++;
            }
        }
        i = 0;
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                rt.addProperty(p, "recommended");
                i++;
            }
        }
        final LinkedList<RecordType> ret = getHeartDBClient().insertRecordType(rt);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
    }

    /**
     * Insert a Property with unit = "unit1". Then insert a RecordType with this
     * Property, but change the unit to "unit2" in the domain of this
     * RecordType. Repeat for a Record with that Property and unit = "unit3".
     * 
     * @author Timm Fitschen
     * @throws IOException
     */
    @Test
    public void insertRecordTypeWithPropertyAndDifferingUnit() throws IOException {

        // insert Property ap with Unit="unit1"
        final Property ap = new Property("HeartDBClientTest" + getRandString() + "Unit1", "Unit1",
                "integer");
        ap.setUnit("unit1");
        final LinkedList<Property> ret = getHeartDBClient().insertProperty(ap);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);

        // insert RecordType with Property ap with Unit="unit2"
        RecordType rt = new RecordType("HeartDBClientTest" + getRandString() + "Unit2", "Unit2");
        for (final Property p : ret) {
            assertTrue(p.getUnit().equals("unit1"));
            p.setUnit("unit2");
            rt.addProperty(p, "obligatory");
        }

        final LinkedList<RecordType> retRt = getHeartDBClient().insertRecordType(rt);
        assertNotNull(retRt);
        assertEquals(1, retRt.size());
        assertNotNull(retRt.getFirst().getMessages());
        assertEquals(0, retRt.getFirst().getMessages().size());
        assertNotNull(retRt.getFirst().getId());
        assertTrue(retRt.getFirst().getId() >= 100);

        for (final RecordType rtNew : retRt) {
            assertTrue(rtNew.getProperty(ap.getName()).getUnit().equals("unit2"));
        }
        rt = retRt.getFirst();

        // insert Record with unit = Unit3
        final Record rec = new Record("HeartDBClientTest" + getRandString() + "Unit3", "Unit3");
        rt.setCuid(null);
        rec.addParent(rt);
        for (final Property p : rt.getProperties()) {
            assertTrue(p.getUnit().equals("unit2"));
            p.setUnit("unit3");
            rec.addProperty(p);
        }
        final LinkedList<Record> retRec = getHeartDBClient().insertRecord(rec);

        assertNotNull(retRec);
        assertEquals(1, retRec.size());
        assertNotNull(retRec.getFirst().getMessages());
        assertEquals(0, retRec.getFirst().getMessages().size());
        assertNotNull(retRec.getFirst().getId());
        assertTrue(retRec.getFirst().getId() >= 100);

        for (final Record recNew : retRec) {
            assertTrue(recNew.getProperty(ap.getName()).getUnit().equals("unit3"));
        }

    }

    @Test
    public void insertRecordTypeWithTwoParents() {
        // insert good recordType
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final RecordType parent1 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        int i = 0;
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent1.addProperty(p, "obligatory");
                i++;
            }
        }
        i = 0;
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent1.addProperty(p, "recommended");
                i++;
            }
        }
        LinkedList<RecordType> ret = getHeartDBClient().insertRecordType(parent1);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        final int parId1 = ret.getFirst().getId();

        // insert good recordType
        final RecordType parent2 = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        i = 0;
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent2.addProperty(p, "obligatory");
                i++;
            }
        }
        i = 0;
        while (i < 3) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent2.addProperty(p, "recommended");
                i++;
            }
        }
        ret = getHeartDBClient().insertRecordType(parent2);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        final int parId2 = ret.getFirst().getId();

        final RecordType rt = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        rt.addParent(parId1);
        rt.addParent(parId2);
        for (final Property p : parent1.getProperties()) {
            if (p.getImportance().equalsIgnoreCase("OBLIGATORY")) {
                rt.addProperty(p);
            }
        }
        for (final Property p : parent2.getProperties()) {
            if (p.getImportance().equalsIgnoreCase("OBLIGATORY")) {
                rt.addProperty(p);
            }
        }
        ret = getHeartDBClient().insertRecordType(rt);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        for (final Message m : ret.getFirst().getMessages()) {
            if (m.getType().equals("Error")) {
                junit.framework.Assert.fail("Error detected: " + m.getDescription());
            }
        }
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
    }

    /**
     * Test PAR_OBL_PROP_PRESENT rule: insert Parent with one obl. Property and
     * a Child that does not have it.
     * 
     * @author Timm Fitschen
     */
    @Test
    public void insertRecordTypeBreakingParOblPropPresentRule() {
        final LinkedList<Property> properties = getHeartDBClient().retrieveProperty("all");
        final RecordType parent = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");

        // add obligatory property
        while (true) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent.addProperty(p, "obligatory");
                break;
            }
        }

        // add recommended property
        while (true) {
            final Property p = properties.get(getRand().nextInt(properties.size()));
            if (p.getId() < 100) {
                continue;
            } else {
                parent.addProperty(p, "recommended");
                break;
            }
        }

        // insert parent
        LinkedList<RecordType> ret = getHeartDBClient().insertRecordType(parent);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());
        assertEquals(0, ret.getFirst().getMessages().size());
        assertNotNull(ret.getFirst().getId());
        assertTrue(ret.getFirst().getId() >= 100);
        final Integer parId = ret.getFirst().getId();
        final RecordType child = new RecordType("HeartDBClientTest" + getRandString(),
                "HeartDBClientTest");
        child.addParent(parId);

        for (final Property p : parent.getProperties()) {
            // add the recommended property but leave out the obligatory one.
            if (p.getImportance().equalsIgnoreCase("RECOMMENDED")) {
                child.addProperty(p);
            }
        }

        // insert child
        ret = getHeartDBClient().insertRecordType(child);
        assertNotNull(ret);
        assertEquals(1, ret.size());
        assertNotNull(ret.getFirst().getMessages());

        // check for message 113 = Obligatory property is missing.
        final LinkedList<Message> messages = ret.getFirst().getMessages();
        boolean found113 = false;
        for (final Message m : messages) {
            if (m.getCode().equals(113)) {
                found113 = true;
            }
        }
        assertTrue(found113);

    }

    @After
    public void writeLog() throws IOException {
        HeartDBClientTest.printLog();
    }
}
