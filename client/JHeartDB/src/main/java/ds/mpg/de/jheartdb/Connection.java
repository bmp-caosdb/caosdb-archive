/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package ds.mpg.de.jheartdb;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;

import ds.mpg.de.jheartdb.log.HTTPLogEntry;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.utils.DBStats;
import ds.mpg.de.jheartdb.utils.Utilities;

public class Connection {
    private Logger logger = null;
    private String hostname;
    private int port = 8122;
    private String basepath;
    private String username;
    private String password;
    private String authToken = null;

    /**
     * Set configuration for the Client. This consists of information necessary
     * to connect to the database.
     * 
     * @param hostname
     * @param port
     * @param basepath
     * @throws IOException
     * @throws SecurityException
     */
    /**
     * Set configuration for the Client. This consists of information necessary
     * to connect to the database.
     * 
     * @param hostname
     * @param port
     * @param basepath
     * @throws IOException
     * @throws SecurityException
     */
    protected Connection configureConnection(final String hostname, final int port,
            final String basepath, final String username, final String password) {
        logger = Logger.getLogger(Connection.class.getName());
        FileHandler fh;
        try {
            fh = new FileHandler("connection.log");
            logger.addHandler(fh);
        } catch (final SecurityException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (final IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        this.hostname = hostname;
        this.port = port;
        this.basepath = basepath;
        this.username = username;
        try {
            this.password = Utilities.hashPassword(password);
        } catch (final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            return login();
        } catch (final Exception e) {
            e.printStackTrace();
            return this;
        }
    }

    private String solveLoginChallenge(String authToken) throws UnsupportedEncodingException,
            NoSuchAlgorithmException {
        authToken = URLDecoder.decode(authToken, "UTF-8");
        if (authToken.toLowerCase().matches(".*mode\\s*=\\s*login.*")) {
            final String oldHash = authToken.toLowerCase().split("hash=", 2)[1].toLowerCase()
                    .split(";|\\}", 2)[0].trim();
            final String newHash = Utilities.hashPassword(oldHash + password);
            authToken = authToken.replaceAll("[hH][aA][sS][Hh]\\s*=\\s*[0-9a-fA-F]*", "hash="
                    + newHash);
            authToken = authToken.replaceAll("^\\s*AuthToken\\s*=\\s*\\{", "");
            authToken = authToken.split("\\}[^\\}]*$")[0];
            return "AuthToken=" + URLEncoder.encode("{" + authToken + "}", "UTF-8");
        }
        return URLEncoder.encode(authToken, "UTF-8");
    }

    public Connection login() throws SocketException, IllegalStateException, URISyntaxException,
            IOException, JDOMException, InterruptedException, NoSuchAlgorithmException {
        sendGetRequest("login?username=" + username);
        if (authToken != null) {
            System.err.println("authToken: " + authToken);
            authToken = solveLoginChallenge(authToken);
            System.err.println("authToken: " + authToken);
            try {
                sendPostRequest("login", new StringEntity(""));
            } catch (final IllegalStateException e) {
                System.err.println("login failed!!!");
                e.printStackTrace();
            }
        }
        return this;
    }

    private static LinkedList<HttpClient> clientPool = new LinkedList<HttpClient>();
    private static int maxConnections = 10;
    private static int cacheConnections = 3;
    private static int conCount = 1;

    private static HttpClient getClient() throws InterruptedException {
        synchronized (clientPool) {
            if (!clientPool.isEmpty()) {
                return clientPool.removeFirst();
            } else {
                if (conCount < maxConnections) {
                    conCount++;

                    final HttpClient ret = new DefaultHttpClient();

                    final HttpParams params = ret.getParams();
                    params.setParameter(CoreProtocolPNames.USER_AGENT, "JHeartDB");
                    HttpClientParams.setCookiePolicy(params, CookiePolicy.IGNORE_COOKIES);
                    return ret;
                }
            }
        }
        Thread.sleep(1000);
        return getClient();
    }

    private static void releaseClient(final HttpClient client) {
        synchronized (clientPool) {
            if (clientPool.size() < cacheConnections) {
                clientPool.add(client);
            } else {
                conCount--;
            }
        }
    }

    private String getURI() throws URISyntaxException {
        return new URIBuilder().setScheme("http").setHost(hostname).setPort(port)
                .setPath("/" + basepath + "/").build().toString();
    }

    protected Document sendGetRequest(final String uriEntitySegment) throws URISyntaxException,
            ClientProtocolException, SocketException, IOException, IllegalStateException,
            JDOMException, InterruptedException {
        // TODO retry should be customizable some time
        int retry;
        retry = 0;
        final String fulluri = getURI() + uriEntitySegment;

        final HttpGet getRequest = new HttpGet(fulluri);

        if (authToken != null) {
            getRequest.setHeader("Cookie", authToken);
        }
        getRequest.setHeader("crequestid", Long.toHexString(Thread.currentThread().getId()));

        final HTTPLogEntry logEntry = new HTTPLogEntry("GET (Thread "
                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                getRequest.getAllHeaders(), null, null, null);
        HeartDBClient.getInstance().addLogEntry(logEntry);

        DBStats.getInstance().startMeasureRequestTotal();
        final HttpClient client = getClient();
        HttpResponse resp = null;

        // retry request if SocketException
        int i = 0;
        while (true) {
            try {
                resp = client.execute(getRequest);
                break;
            } catch (final SocketException e) {
                if (retry != 0) {
                    i++;
                    if (i > retry) {
                        DBStats.getInstance().finishMeasureRequestTotal();
                        releaseClient(client);
                        throw e;
                    }
                }
            } catch (final NoHttpResponseException e) {
                if (retry != 0) {
                    i++;
                    if (i > retry) {
                        DBStats.getInstance().finishMeasureRequestTotal();
                        releaseClient(client);
                        throw e;
                    }
                }
            } catch (final SocketTimeoutException e) {
                HeartDBClient.getInstance().addLogEntry(
                        new HTTPLogEntry("GET Exception (Thread "
                                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                                null, "SoTimeout reached: "
                                        + client.getParams()
                                                .getParameter(CoreConnectionPNames.SO_TIMEOUT)
                                                .toString(), null, null));
                DBStats.getInstance().finishMeasureRequestTotal();
                releaseClient(client);
                return null;
            }

        }

        final HttpEntity entity = resp.getEntity();
        DBStats.getInstance().finishMeasureRequestTotal();

        final Document doc = Utilities.inputStream2Document(entity.getContent());
        final Header cookie = resp.getFirstHeader("Set-Cookie");
        if (cookie != null) {
            authToken = cookie.getValue();
        }
        getRequest.releaseConnection();
        releaseClient(client);

        logEntry.setRespEntry(doc);
        logEntry.setRespHeader(resp.getAllHeaders());
        return doc;
    }

    protected Document downloadFile(final File file, final java.io.File target)
            throws ClientProtocolException, IOException, URISyntaxException, IllegalStateException,
            JDOMException, InterruptedException {
        final String fulluri = getURI() + "FileSystem/" + file.getPath();

        final HttpGet getRequest = new HttpGet(fulluri);
        if (authToken != null) {
            getRequest.setHeader("Cookie", authToken);
        }

        final HTTPLogEntry logEntry = new HTTPLogEntry("GET (Thread "
                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                getRequest.getAllHeaders(), null, null, null);
        HeartDBClient.getInstance().addLogEntry(logEntry);

        final HttpClient client = getClient();
        final HttpResponse resp = client.execute(getRequest);
        final int status = resp.getStatusLine().getStatusCode();
        final HttpEntity respEntity = resp.getEntity();

        if (status != 200) {
            final Document doc = Utilities.inputStream2Document(respEntity.getContent());
            getRequest.releaseConnection();
            releaseClient(client);

            logEntry.setRespEntry(doc);
            logEntry.setRespHeader(resp.getAllHeaders());
            return doc;
        } else {

            final FileOutputStream fileOutputStream = new FileOutputStream(target);
            int b;
            int i = 0;
            final InputStream in = respEntity.getContent();
            while ((b = in.read()) != -1) {
                fileOutputStream.write(b);
                i++;
                if (i > 1000) {
                    fileOutputStream.flush();
                }
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            getRequest.releaseConnection();
            releaseClient(client);

            logEntry.setRespBody("HTTP Status: " + Integer.toString(status));
            logEntry.setRespHeader(resp.getAllHeaders());

            file.setFile(target);
        }
        return null;
    }

    protected Document sendDeleteRequest(final String uriEntitySegment) throws URISyntaxException,
            ClientProtocolException, IOException, IllegalStateException, JDOMException,
            InterruptedException {
        final String fulluri = getURI() + uriEntitySegment;

        final HttpDelete deleteRequest = new HttpDelete(fulluri);
        if (authToken != null) {
            deleteRequest.setHeader("Cookie", authToken);
        }

        final HTTPLogEntry logEntry = new HTTPLogEntry("DELETE (Thread "
                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                deleteRequest.getAllHeaders(), null, null, null);
        HeartDBClient.getInstance().addLogEntry(logEntry);

        DBStats.getInstance().startMeasureRequestTotal();
        final HttpClient client = getClient();
        final HttpResponse resp = client.execute(deleteRequest);
        final HttpEntity entity = resp.getEntity();
        DBStats.getInstance().finishMeasureRequestTotal();

        final Document doc = Utilities.inputStream2Document(entity.getContent());
        final Header cookie = resp.getFirstHeader("Set-Cookie");
        if (cookie != null) {
            authToken = cookie.getValue();
        }
        deleteRequest.releaseConnection();
        releaseClient(client);

        logEntry.setRespEntry(doc);
        logEntry.setRespHeader(resp.getAllHeaders());
        return doc;
    }

    protected Document sendPostRequest(final String uriEntitySegment, final HttpEntity postEntity)
            throws IllegalStateException, JDOMException, IOException, URISyntaxException,
            InterruptedException {
        final String fulluri = getURI() + uriEntitySegment;

        final HttpPost postRequest = new HttpPost(fulluri);
        if (authToken != null) {
            postRequest.setHeader("Cookie", authToken);
        }
        postRequest.setEntity(postEntity);

        final HTTPLogEntry logEntry = new HTTPLogEntry("POST (Thread "
                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                postRequest.getAllHeaders(), null, postRequest.getEntity(), null);
        HeartDBClient.getInstance().addLogEntry(logEntry);

        DBStats.getInstance().startMeasureRequestTotal();
        final HttpClient client = getClient();
        final HttpResponse resp = client.execute(postRequest);
        final HttpEntity respEntity = resp.getEntity();
        DBStats.getInstance().finishMeasureRequestTotal();

        Document doc = null;
        try {
            doc = Utilities.inputStream2Document(respEntity.getContent());
        } catch (final JDOMParseException e) {
            e.printStackTrace();
        }
        final Header cookie = resp.getFirstHeader("Set-Cookie");
        if (cookie != null) {
            authToken = cookie.getValue();
        }
        postRequest.releaseConnection();
        releaseClient(client);

        logEntry.setRespEntry(doc);
        logEntry.setRespHeader(resp.getAllHeaders());
        if (resp.getStatusLine().getStatusCode() != 200) {
            throw new IllegalStateException("Status:" + resp.getStatusLine().toString());
        }
        return doc;
    }

    protected Document sendPutRequest(final String uriEntitySegment, final HttpEntity postEntity)
            throws IllegalStateException, JDOMException, IOException, URISyntaxException,
            InterruptedException {
        // TODO make retry customizable
        int retry;
        retry = 0;
        final String fulluri = getURI() + uriEntitySegment;

        final HttpPut putRequest = new HttpPut(fulluri);
        if (authToken != null) {
            putRequest.setHeader("Cookie", authToken);
        }
        putRequest.setHeader("crequestid", Long.toHexString(Thread.currentThread().getId()));
        putRequest.setEntity(postEntity);

        final HTTPLogEntry logEntry = new HTTPLogEntry("PUT (Thread "
                + Long.toHexString(Thread.currentThread().getId()) + ")", fulluri,
                putRequest.getAllHeaders(), null, putRequest.getEntity(), null);
        HeartDBClient.getInstance().addLogEntry(logEntry);

        DBStats.getInstance().startMeasureRequestTotal();
        final HttpClient client = getClient();
        HttpResponse resp = null;

        // retry request if SocketException
        int i = 0;
        while (true) {
            try {
                resp = client.execute(putRequest);
                break;
            } catch (final SocketException e) {
                if (retry != 0) {
                    i++;
                    if (i > retry) {
                        throw e;
                    }
                }
            } catch (final NoHttpResponseException e) {
                if (retry != 0) {
                    i++;
                    if (i > retry) {
                        throw e;
                    }
                }
            }
        }

        final StringBuilder builder = new StringBuilder();
        builder.append("============================================================");
        builder.append("Thread " + Long.toHexString(Thread.currentThread().getId()) + ":\n");
        builder.append("Response headers:\n");
        for (final Header h : resp.getAllHeaders()) {
            builder.append(h.getName());
            builder.append(": ");
            builder.append(h.getValue());
            builder.append("\n");
        }
        builder.append("\n");
        final HttpEntity respEntity = resp.getEntity();
        DBStats.getInstance().finishMeasureRequestTotal();

        final InputStream content = respEntity.getContent();
        builder.append("Content length:");
        builder.append(content.available());
        builder.append("\n");

        final Document doc = Utilities.inputStream2Document(content);
        final Header cookie = resp.getFirstHeader("Set-Cookie");
        if (cookie != null) {
            authToken = cookie.getValue();
        }
        putRequest.releaseConnection();
        releaseClient(client);

        logEntry.setRespEntry(doc);
        logEntry.setRespHeader(resp.getAllHeaders());
        builder.append("============================================================");
        logger.info(builder.toString());
        return doc;
    }

    public boolean canConnect() {
        try {

            final HttpOptions request = new HttpOptions(getURI() + "Info");
            if (authToken != null) {
                request.setHeader("Cookie", authToken);
            }

            final HTTPLogEntry logEntry = new HTTPLogEntry("OPTIONS  (Thread "
                    + Long.toHexString(Thread.currentThread().getId()) + ")", getURI() + "Info",
                    request.getAllHeaders(), null, null, null);
            HeartDBClient.getInstance().addLogEntry(logEntry);

            DBStats.getInstance().startMeasureRequestTotal();
            final HttpClient client = getClient();
            final HttpResponse resp = client.execute(request);
            DBStats.getInstance().finishMeasureRequestTotal();
            final Header cookie = resp.getFirstHeader("Set-Cookie");
            if (cookie != null) {
                authToken = cookie.getValue();
            }
            request.releaseConnection();
            releaseClient(client);

            logEntry.setRespHeader(resp.getAllHeaders());

            return true;
        } catch (final SocketTimeoutException e) {
            // Cannot connect due to timeout
        } catch (final UnknownHostException e) {
            // Cannot find host
        } catch (final HttpHostConnectException e) {
            // Connection rejected
        } catch (final NoHttpResponseException e) {
            // no response (wrong port?)
        } catch (final Exception e) {
            e.printStackTrace();
        }
        DBStats.getInstance().finishMeasureRequestTotal();
        return false;
    }

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

}
