/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-02-11
 */

package ds.mpg.de.jheartdb.utils;

import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 * Performs simple benchmarking on the database.
 *
 * @author salexan
 */
public class DBStats {

    private static DBStats instance = null;

    public static DBStats getInstance() {
        if (instance == null) {
            instance = new DBStats();
        }
        return instance;
    }
    private long timeRequestsTotal = 0;
    private long nRequestsTotal = 0;

    private DBStats() {
    }

    public void setRequestsTotal(long a, long b) {
        setTimeRequestsTotal(a);
        setnRequestsTotal(b);
    }
    private Semaphore sTotal = new Semaphore(1);
    private long lastTime = -1L;

    public void startMeasureRequestTotal() {
        if (sTotal.tryAcquire()) {
            lastTime = new Date().getTime();
        } else {
            // Two simultaneous measurements are not allowed and cause the cancellation of the current one:
            lastTime = -1;
            sTotal.release();
        }
    }

    public void finishMeasureRequestTotal() {
        if (lastTime != -1L) {
            setTimeRequestsTotal(getTimeRequestsTotal() + (new Date().getTime() - lastTime));
            setnRequestsTotal(getnRequestsTotal() + 1);

            lastTime = -1;
            sTotal.release();
        }
    }

    public double getMeanRequestsTotal() {
        return (double) getTimeRequestsTotal() / (double) getnRequestsTotal();
    }

    /**
     * @return the nRequestsTotal
     */
    public long getnRequestsTotal() {
        return nRequestsTotal;
    }

    /**
     * @param nRequestsTotal the nRequestsTotal to set
     */
    public void setnRequestsTotal(long nRequestsTotal) {
        this.nRequestsTotal = nRequestsTotal;
    }

    /**
     * @return the timeRequestsTotal
     */
    public long getTimeRequestsTotal() {
        return timeRequestsTotal;
    }

    /**
     * @param timeRequestsTotal the timeRequestsTotal to set
     */
    public void setTimeRequestsTotal(long timeRequestsTotal) {
        this.timeRequestsTotal = timeRequestsTotal;
    }
}
