/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */
package ds.mpg.de.jheartdb;

import static ds.mpg.de.jheartdb.utils.Utilities.folderIsReachable;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.apache.http.entity.StringEntity;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import ds.mpg.de.jheartdb.log.HTTPLogEntry;
import ds.mpg.de.jheartdb.structures.DBException;
import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.File;
import ds.mpg.de.jheartdb.structures.Info;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.Record;
import ds.mpg.de.jheartdb.structures.RecordType;
import ds.mpg.de.jheartdb.utils.Utilities;

/**
 * CLIENT API FOR JAVA (JHeartDB)
 * 
 * Singleton class which manages queries to the heartdb server.
 * 
 * @author salexan
 */
public class HeartDBClient implements HeartDBClientInterface {

    public class DropOffBoxNotReachableException extends Exception {

        /**
		 * 
		 */
        private static final long serialVersionUID = -8567600274291196838L;

    }

    public static final int AUTOMATIC = -1;
    public static final int HTTP_UPLOAD = 0;
    public static final int DROP_OFF_BOX = 1;

    private static HeartDBClient instance = null;

    public static HeartDBClient getInstance() {

        if (instance == null) {
            instance = new HeartDBClient();
            // Default Configuration:
        }
        return instance;
    }

    private HeartDBClient() {
    }

    private final LinkedList<HTTPLogEntry> log = new LinkedList<HTTPLogEntry>();
    private final Connection connection = new Connection();

    public void configureConnection(final String hostname, final int port, final String basepath,
            final String username, final String password) {
        connection.configureConnection(hostname, port, basepath, username, password);
    }

    @Override
    public void addLogEntry(final HTTPLogEntry entry) {
        synchronized (log) {
            log.add(entry);
        }
    }

    @Override
    public List<HTTPLogEntry> getLogList() {
        return log;
    }

    @Override
    public Document postXML(final String xmlString, final String uriSegment) {
        try {
            final Document document = connection.sendPostRequest(uriSegment, new StringEntity(
                    xmlString, "UTF-8"));

            return document;
        } catch (final Exception ex) {
            showException(ex);
        }
        return null;
    }

    @Override
    public Info getServerInfo() {
        try {
            final Document doc = connection.sendGetRequest("Info");
            if (doc == null) {
                return null;
            }

            final Info info = new Info();
            info.setFromElement(doc.getRootElement().getChild("Info"));

            return info;
        } catch (final IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private void showException(final Exception ex) {
        Logger.getLogger(HeartDBClient.class.getName()).log(Level.SEVERE, null, ex);
        String messageString = "<html><b>Exception:</b><br>" + ex.toString();
        messageString += "</html>";
        JOptionPane.showMessageDialog(null, messageString, "Exception", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public boolean canConnect() {
        return connection.canConnect();
    }

    @Override
    public List<DBException> findExceptions(final Element e, final HashSet<Integer> ignoreExceptions) {
        final ArrayList<DBException> exceptions = new ArrayList<DBException>();
        for (final Element ch : e.getChildren()) {
            if (ch.getName().equals("Error")
                    && !ignoreExceptions.contains(Integer.parseInt(ch.getAttributeValue("code")))) {
                exceptions.add(new DBException(ch.getAttributeValue("code"), ch
                        .getAttributeValue("description")));
            }
            exceptions.addAll(findExceptions(ch, ignoreExceptions));
        }
        return exceptions;
    }

    @Override
    public List<DBException> findExceptions(final Element e, final int[] ignoreExceptions) {
        final HashSet<Integer> ie = new HashSet<Integer>(ignoreExceptions.length);
        for (final int x : ignoreExceptions) {
            ie.add(x);
        }
        return findExceptions(e, ie);
    }

    private Document insertEntities(final Iterable<? extends Entity> inserts,
            final String uriSegment) {
        if (inserts == null) {
            return null;
        }

        final Element requestRoot = new Element("Insert");
        final Document requestDoc = new Document(requestRoot);
        int negId = -1000000;
        for (final Entity e : inserts) {
            if (e.getId() == null) {
                e.setId(negId--);
            }
        }
        for (final Entity e : inserts) {
            requestRoot.addContent(e.createElement());
        }

        try {
            return connection
                    .sendPostRequest(uriSegment, Utilities.Document2HttpEntity(requestDoc));

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document insertFilesViaHttp(final LinkedList<File> uploadList, final String uriSegment) {
        if (uploadList == null || uploadList.isEmpty()) {
            return null;
        }
        try {

            final Element insertRootElement = new Element("Insert");

            return connection.sendPostRequest(uriSegment,
                    Utilities.FilesList2MultipartEntity(insertRootElement, uploadList));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document updateFilesViaHttp(final List<File> files, final String uriSegment) {
        if (files == null || files.isEmpty()) {
            return null;
        }
        try {
            final Element updateRootElement = new Element("Update");
            updateRootElement.setAttribute("request",
                    Long.toHexString(Thread.currentThread().getId()));
            final LinkedList<File> transmitFiles = new LinkedList<File>();

            // decide whether the file has to be transmitted. If merely the
            // "meta-data" has changed while the file remains unchanged, the
            // file does not have to be transmitted.
            for (final File f : files) {

                // if the file is null, it is obviously not intended to be
                // transmitted.
                if (f.getFile() == null) {
                    updateRootElement.addContent(f.createElement());
                    continue;
                }

                // if the checksum comparison passes the file doesn't have to be
                // uploaded.
                if (f.checksumComparison()) {
                    updateRootElement.addContent(f.createElement());
                    continue;
                }

                transmitFiles.add(f);
            }

            return connection.sendPutRequest(uriSegment,
                    Utilities.FilesList2MultipartEntity(updateRootElement, transmitFiles));

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document retrieveEntities(final String uriSegment) {
        if (uriSegment == null || uriSegment.equals("")) {
            return null;
        }
        try {
            return connection.sendGetRequest(uriSegment);

        } catch (final Exception e) {
            e.printStackTrace();
        }

        System.exit(1);
        return null;
    }

    private Document updateEntities(final Iterable<? extends Entity> updates,
            final String uriSegment) {
        if (updates == null) {
            return null;
        }
        final Element requestRoot = new Element("Update");
        final Document requestDoc = new Document(requestRoot);
        for (final Entity e : updates) {
            requestRoot.addContent(e.createElement());
        }

        try {
            return connection.sendPutRequest(uriSegment, Utilities.Document2HttpEntity(requestDoc));

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document deleteEntities(final String uriSegment) {
        if (uriSegment == null || uriSegment.equals("")) {
            return null;
        }
        try {
            return connection.sendDeleteRequest(uriSegment);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public LinkedList<Record> retrieveRecord(final Integer[] ids) {
        return retrieveRecord(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<Record> retrieveRecord(final String[] names) {
        return retrieveRecord(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<Record> retrieveRecord(final Integer id) {
        return retrieveRecord(Integer.toString(id));
    }

    @Override
    public LinkedList<Record> retrieveRecord(final Integer id, final String name) {
        return retrieveRecord(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<Record> retrieveRecord(final Iterable<Integer> ids,
            final Iterable<String> names) {
        return retrieveRecord(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<Record> retrieveRecord(final String str) {
        final Document doc = retrieveEntities("Record/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<Record> ret = new LinkedList<Record>();
        for (final Element e : root.getChildren("Record")) {
            ret.add(new Record(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Record> deleteRecord(final Integer[] ids) {
        return deleteRecord(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<Record> deleteRecord(final String[] names) {
        return deleteRecord(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<Record> deleteRecord(final Integer id) {
        return deleteRecord(Integer.toString(id));
    }

    @Override
    public LinkedList<Record> deleteRecord(final Integer id, final String name) {
        return deleteRecord(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<Record> deleteRecord(final Iterable<Integer> ids, final Iterable<String> names) {
        return deleteRecord(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<Record> deleteRecord(final String str) {
        final Document doc = deleteEntities("Record/" + str);
        if (doc == null) {
            return null;
        }
        System.out.println(Utilities.Document2String(doc));
        final Element root = doc.getRootElement();
        final LinkedList<Record> ret = new LinkedList<Record>();
        for (final Element e : root.getChildren("Record")) {
            ret.add(new Record(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Record> updateRecord(final Iterable<Record> updates) {
        final Document responseDoc = updateEntities(updates, "Record");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Record> ret = new LinkedList<Record>();
        for (final Element e : responseRoot.getChildren("Record")) {
            ret.add(new Record(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Record> insertRecord(final Record insert) {
        final ArrayList<Record> inserts = new ArrayList<Record>(1);
        inserts.add(insert);
        return insertRecord(inserts);
    }

    @Override
    public LinkedList<Record> insertRecord(final Iterable<Record> inserts) {
        final Document responseDoc = insertEntities(inserts, "Record");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Record> ret = new LinkedList<Record>();
        for (final Element e : responseRoot.getChildren("Record")) {
            ret.add(new Record(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final Integer[] ids) {
        return retrieveRecordType(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final String[] names) {
        return retrieveRecordType(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final Integer id) {
        return retrieveRecordType(Integer.toString(id));
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final Integer id, final String name) {
        return retrieveRecordType(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final Iterable<Integer> ids,
            final Iterable<String> names) {
        return retrieveRecordType(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<RecordType> retrieveRecordType(final String str) {
        final Document doc = retrieveEntities("RecordType/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<RecordType> ret = new LinkedList<RecordType>();
        for (final Element e : root.getChildren("RecordType")) {
            ret.add(new RecordType(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final Integer[] ids) {
        return deleteRecordType(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final String[] names) {
        return deleteRecordType(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final Integer id) {
        return deleteRecordType(Integer.toString(id));
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final Integer id, final String name) {
        return deleteRecordType(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final Iterable<Integer> ids,
            final Iterable<String> names) {
        return deleteRecordType(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<RecordType> deleteRecordType(final String str) {
        final Document doc = deleteEntities("RecordType/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<RecordType> ret = new LinkedList<RecordType>();
        for (final Element e : root.getChildren("RecordType")) {
            ret.add(new RecordType(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<RecordType> updateRecordType(final Iterable<RecordType> updates) {
        final Document responseDoc = updateEntities(updates, "RecordType");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<RecordType> ret = new LinkedList<RecordType>();
        for (final Element e : responseRoot.getChildren("RecordType")) {
            ret.add(new RecordType(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<RecordType> insertRecordType(final RecordType insert) {
        final ArrayList<RecordType> inserts = new ArrayList<RecordType>(1);
        inserts.add(insert);
        return insertRecordType(inserts);
    }

    @Override
    public LinkedList<RecordType> insertRecordType(final Iterable<RecordType> inserts) {
        final Document responseDoc = insertEntities(inserts, "RecordType");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<RecordType> ret = new LinkedList<RecordType>();
        for (final Element e : responseRoot.getChildren("RecordType")) {
            ret.add(new RecordType(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<File> retrieveFile(final Integer[] ids) {
        return retrieveFile(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<File> retrieveFile(final String[] names) {
        return retrieveFile(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<File> retrieveFile(final Integer id) {
        return retrieveFile(Integer.toString(id));
    }

    @Override
    public LinkedList<File> retrieveFile(final Integer id, final String name) {
        return retrieveFile(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<File> retrieveFile(final Iterable<Integer> ids, final Iterable<String> names) {
        return retrieveFile(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<File> retrieveFile(final String str) {
        final Document doc = retrieveEntities("File/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<File> ret = new LinkedList<File>();
        for (final Element e : root.getChildren("File")) {
            ret.add(new File(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<File> deleteFile(final Integer[] ids) {
        return deleteFile(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<File> deleteFile(final String[] names) {
        return deleteFile(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<File> deleteFile(final Integer id) {
        return deleteFile(Integer.toString(id));
    }

    @Override
    public LinkedList<File> deleteFile(final Integer id, final String name) {
        return deleteFile(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<File> deleteFile(final Iterable<Integer> ids, final Iterable<String> names) {
        return deleteFile(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<File> deleteFile(final String str) {
        final Document doc = deleteEntities("File/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<File> ret = new LinkedList<File>();
        for (final Element e : root.getChildren("File")) {
            ret.add(new File(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<File> updateFile(final Iterable<File> updates) {
        return updateFile(updates, AUTOMATIC);
    }

    @Override
    public LinkedList<File> updateFile(final File update) {
        return updateFile(update, AUTOMATIC);
    }

    @Override
    public LinkedList<File> updateFile(final File update, final int method) {
        final LinkedList<File> updates = new LinkedList<File>();
        updates.add(update);
        return updateFile(updates, method);
    }

    @Override
    public LinkedList<File> updateFile(final Iterable<File> updates, final int method) {
        // determine if the file is expected to be already in the dropOffBox.
        Info info = null;
        LinkedList<File> pickupList = null;
        LinkedList<File> uploadList = null;
        for (final File f : updates) {

            switch (method) {
            case AUTOMATIC:
                if (f.getPickup() != null) {
                    // it's expected to be in the dropOffBox.

                    if (pickupList == null) {
                        pickupList = new LinkedList<File>();
                    }
                    pickupList.add(f);
                } else if (f.getFile() != null) {
                    if (info == null) {
                        info = getServerInfo();
                    }
                    final String dropOffBoxPath = info.getDropOffBoxPath();
                    if (folderIsReachable(dropOffBoxPath, connection.getHostname())) {
                        if (f.cpToDropOffBox(dropOffBoxPath)) {
                            if (pickupList == null) {
                                pickupList = new LinkedList<File>();
                            }
                            pickupList.add(f);
                        }
                    }
                } else {
                    if (uploadList == null) {
                        uploadList = new LinkedList<File>();
                    }

                    // fall back to HTTP upload
                    uploadList.add(f);
                }
                break;
            case HTTP_UPLOAD:
                if (uploadList == null) {
                    uploadList = new LinkedList<File>();
                }
                uploadList.add(f);
                break;
            case DROP_OFF_BOX:
                if (pickupList == null) {
                    pickupList = new LinkedList<File>();
                }
                if (f.getPickup() != null) {
                    // it's expected to be in the dropOffBox.
                    pickupList.add(f);
                } else if (f.getFile() != null) {
                    if (info == null) {
                        info = getServerInfo();
                    }
                    final String dropOffBoxPath = info.getDropOffBoxPath();
                    if (folderIsReachable(dropOffBoxPath, connection.getHostname())) {
                        if (f.cpToDropOffBox(dropOffBoxPath)) {
                            pickupList.add(f);
                        }
                    }
                } else {
                    // Nobody knows why this happens. But the pickup is forced.
                    pickupList.add(f);
                }
                break;
            default:
                return null;
            }
        }

        final LinkedList<File> ret = new LinkedList<File>();

        // file pick up
        if (pickupList != null) {
            final Document responseDocPickup = updateEntities(pickupList, "FilesDropOff");
            if (responseDocPickup != null) {

                final Element responseRootPickup = responseDocPickup.getRootElement();

                for (final Element e : responseRootPickup.getChildren("File")) {
                    ret.add(new File(e));
                }
            }
        }

        // file upload
        if (uploadList != null) {
            final Document responseDocUpload = updateFilesViaHttp(uploadList, "File");
            if (responseDocUpload != null) {

                final Element responseRootUpload = responseDocUpload.getRootElement();

                for (final Element e : responseRootUpload.getChildren("File")) {
                    ret.add(new File(e));
                }
            }
        }

        if (!ret.isEmpty()) {
            return ret;
        }
        return null;

    }

    /**
     * Insert a new File Object. Force to transfer the corresponding file or
     * folder via the method denoted by the flags HTTP_UPLOAD, or DROP_OFF_BOX.
     * In case of the AUTOMATIC flag, this method tries to use the DropOffBox
     * and falls back to HTTP file upload if the DropOffBox is not reachable
     * from the local file system.
     * 
     * @param inserts
     *            the File objects to be inserted
     * @param method
     *            flag to force using a certain method
     * @author Timm Fitschen
     * @throws DropOffBoxNotReachableException
     */
    @Override
    public LinkedList<File> insertFile(final Iterable<File> inserts, final int method)
            throws DropOffBoxNotReachableException {

        // determine if the file is expected to be already in the dropOffBox.
        Info info = null;
        LinkedList<File> pickupList = null;
        LinkedList<File> uploadList = null;
        for (final File f : inserts) {

            switch (method) {
            case AUTOMATIC:
                if (f.getPickup() != null) {
                    // it's expected to be in the dropOffBox.

                    if (pickupList == null) {
                        pickupList = new LinkedList<File>();
                    }
                    pickupList.add(f);
                    break;
                } else if (f.getFile() != null) {
                    if (info == null) {
                        info = getServerInfo();
                    }
                    final String dropOffBoxPath = info.getDropOffBoxPath();
                    if (folderIsReachable(dropOffBoxPath, connection.getHostname())) {
                        if (f.cpToDropOffBox(dropOffBoxPath)) {
                            if (pickupList == null) {
                                pickupList = new LinkedList<File>();
                            }
                            pickupList.add(f);
                            break;
                        }
                    }
                }
                // fall back to HTTP upload
                if (uploadList == null) {
                    uploadList = new LinkedList<File>();
                }

                uploadList.add(f);
                break;
            case HTTP_UPLOAD:
                if (uploadList == null) {
                    uploadList = new LinkedList<File>();
                }
                uploadList.add(f);
                break;
            case DROP_OFF_BOX:
                if (pickupList == null) {
                    pickupList = new LinkedList<File>();
                }
                if (f.getPickup() != null) {
                    // it's expected to be in the dropOffBox.
                    pickupList.add(f);
                } else if (f.getFile() != null) {
                    if (info == null) {
                        info = getServerInfo();
                    }
                    final String dropOffBoxPath = info.getDropOffBoxPath();
                    if (folderIsReachable(dropOffBoxPath, connection.getHostname())) {
                        if (f.cpToDropOffBox(dropOffBoxPath)) {
                            pickupList.add(f);
                        }
                    } else {
                        throw new DropOffBoxNotReachableException();
                    }
                } else {
                    // Nobody knows why this happens. But the pickup is forced.
                    pickupList.add(f);
                }
                break;
            default:
                return null;
            }
        }

        final LinkedList<File> ret = new LinkedList<File>();

        // file pick up
        if (pickupList != null) {
            final Document responseDocPickup = insertEntities(pickupList, "FilesDropOff");
            if (responseDocPickup != null) {

                final Element responseRootPickup = responseDocPickup.getRootElement();

                for (final Element e : responseRootPickup.getChildren("File")) {
                    ret.add(new File(e));
                }
            }
        }

        // file upload
        if (uploadList != null) {
            final Document responseDocUpload = insertFilesViaHttp(uploadList, "File");
            if (responseDocUpload != null) {

                final Element responseRootUpload = responseDocUpload.getRootElement();

                for (final Element e : responseRootUpload.getChildren("File")) {
                    ret.add(new File(e));
                }
            }
        }

        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    /**
     * Insert a new File Object. The corresponding file or folder is transfered
     * to server via the DropOffBox or via HTTP file upload. This method tries
     * to use the DropOffBox and falls back to HTTP file upload if the
     * DropOffBox is not reachable from the local file system.
     * 
     * @param inserts
     *            the File objects to be inserted
     * @author Timm Fitschen
     * @throws DropOffBoxNotReachableException
     */
    @Override
    public LinkedList<File> insertFile(final Iterable<File> inserts)
            throws DropOffBoxNotReachableException {
        return insertFile(inserts, -1);
    }

    @Override
    public LinkedList<File> insertFile(final File insert) throws DropOffBoxNotReachableException {
        final LinkedList<File> inserts = new LinkedList<File>();
        inserts.add(insert);
        return insertFile(inserts, -1);
    }

    @Override
    public LinkedList<File> insertFile(final File insert, final int method)
            throws DropOffBoxNotReachableException {
        final LinkedList<File> inserts = new LinkedList<File>();
        inserts.add(insert);
        return insertFile(inserts, method);
    }

    @Override
    public LinkedList<Property> retrieveProperty(final Integer[] ids) {
        return retrieveProperty(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<Property> retrieveProperty(final String[] names) {
        return retrieveProperty(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<Property> retrieveProperty(final Integer id) {
        return retrieveProperty(Integer.toString(id));
    }

    @Override
    public LinkedList<Property> retrieveProperty(final Integer id, final String name) {
        return retrieveProperty(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<Property> retrieveProperty(final Iterable<Integer> ids,
            final Iterable<String> names) {
        return retrieveProperty(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<Property> retrieveProperty(final String str) {
        final Document doc = retrieveEntities("Property/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<Property> ret = new LinkedList<Property>();
        for (final Element e : root.getChildren("Property")) {
            ret.add(new Property(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Property> deleteProperty(final Integer[] ids) {
        return deleteProperty(Utilities.buildEntityURISegment(ids));
    }

    @Override
    public LinkedList<Property> deleteProperty(final String[] names) {
        return deleteProperty(Utilities.buildEntityURISegment(names));
    }

    @Override
    public LinkedList<Property> deleteProperty(final Integer id) {
        return deleteProperty(Integer.toString(id));
    }

    @Override
    public LinkedList<Property> deleteProperty(final Integer id, final String name) {
        return deleteProperty(Utilities.buildEntityURISegment(id, name));
    }

    @Override
    public LinkedList<Property> deleteProperty(final Iterable<Integer> ids,
            final Iterable<String> names) {
        return deleteProperty(Utilities.buildEntityURISegment(ids, names));
    }

    @Override
    public LinkedList<Property> deleteProperty(final String str) {
        final Document doc = deleteEntities("Property/" + str);
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<Property> ret = new LinkedList<Property>();
        for (final Element e : root.getChildren("Property")) {
            ret.add(new Property(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Property> updateProperty(final Iterable<Property> updates) {
        final Document responseDoc = updateEntities(updates, "Property");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Property> ret = new LinkedList<Property>();
        for (final Element e : responseRoot.getChildren("Property")) {
            ret.add(new Property(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    @Override
    public LinkedList<Property> insertProperty(final Property insert) {
        final ArrayList<Property> inserts = new ArrayList<Property>(1);
        inserts.add(insert);
        return insertProperty(inserts);
    }

    @Override
    public LinkedList<Property> insertProperty(final Iterable<Property> inserts) {
        final Document responseDoc = insertEntities(inserts, "Property");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Property> ret = new LinkedList<Property>();
        for (final Element e : responseRoot.getChildren("Property")) {
            ret.add(new Property(e));
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    /**
     * @param idsOrNames
     * @return LinkedList or deleted Records.
     * @author Timm Fitschen
     */
    @Override
    public LinkedList<Record> deleteRecord(final Iterable<? extends Object> idsOrNames) {
        return deleteRecord(Utilities.buildEntityURISegment(idsOrNames));
    }

    /**
     * @param idsOrNames
     * @return LinkedList or deleted RecordTypes.
     * @author Timm Fitschen
     */
    public LinkedList<RecordType> deleteRecordType(final Iterable<? extends Object> idsOrNames) {
        return deleteRecordType(Utilities.buildEntityURISegment(idsOrNames));
    }

    /**
     * @param idsOrNames
     * @return
     * @author Timm Fitschen
     */
    public LinkedList<Property> deleteProperty(final Iterable<? extends Object> idsOrNames) {
        return deleteProperty(Utilities.buildEntityURISegment(idsOrNames));
    }

    /**
     * Download file from HeartDB's file system and store it to the target.
     * 
     * @param file
     * @param target
     * @return null if everything went correctly. The returning document
     *         contains error messages.
     * @author Timm Fitschen
     * @throws URISyntaxException
     * @throws IOException
     * @throws JDOMException
     * @throws IllegalStateException
     * @throws InterruptedException
     */
    public Document downloadFile(final File file, final java.io.File target) throws IOException,
            URISyntaxException, IllegalStateException, JDOMException, InterruptedException {
        return connection.downloadFile(file, target);
    }

    /**
     * @param p
     * @return
     * @author Timm Fitschen
     */
    public LinkedList<Entity> insertEntity(final Entity... entities) {
        final LinkedList<Entity> inserts = new LinkedList<Entity>();
        for (final Entity entity : entities) {
            inserts.add(entity);
        }
        final Document responseDoc = insertEntities(inserts, "Entity");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Entity> ret = new LinkedList<Entity>();
        for (final Element e : responseRoot.getChildren()) {
            try {
                ret.add(Entity.entityFactory(e));
            } catch (final DBException e1) {
                e1.printStackTrace();
            }
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    /**
     * @param pId
     * @param rtId
     * @return
     * @author Timm Fitschen
     */
    @Override
    public LinkedList<Entity> retrieveEntity(final Integer... ids) {
        final StringBuilder sb = new StringBuilder();
        for (final Integer id : ids) {
            sb.append(id);
            sb.append("&");
        }
        final Document doc = retrieveEntities("Entity/" + sb.toString());
        if (doc == null) {
            return null;
        }

        final Element root = doc.getRootElement();
        final LinkedList<Entity> ret = new LinkedList<Entity>();
        for (final Element e : root.getChildren()) {
            try {
                ret.add(Entity.entityFactory(e));
            } catch (final DBException e1) {
                e1.printStackTrace();
            }
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }

    /**
     * @param entities
     * @return
     * @author Timm Fitschen
     */
    public LinkedList<Entity> updateEntity(final Entity... entities) {
        final LinkedList<Entity> updates = new LinkedList<Entity>();
        for (final Entity entity : entities) {
            updates.add(entity);
        }
        final Document responseDoc = updateEntities(updates, "Entity");
        if (responseDoc == null) {
            return null;
        }

        final Element responseRoot = responseDoc.getRootElement();

        final LinkedList<Entity> ret = new LinkedList<Entity>();
        for (final Element e : responseRoot.getChildren()) {
            try {
                ret.add(Entity.entityFactory(e));
            } catch (final DBException e1) {
                e1.printStackTrace();
            }
        }
        if (!ret.isEmpty()) {
            return ret;
        }
        return null;
    }
}
