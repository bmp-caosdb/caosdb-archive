/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb.structures;

import java.util.List;

/**
 *
 * @author salexan
 */
public class DBExceptions extends Exception {

    private List<DBException> exceptions;

    /**
     * Creates a new instance of
     * <code>DBExceptions</code> without detail message.
     */
    public DBExceptions() {
    }

    public static DBExceptions createDBExceptions(List<DBException> exceptions) {
        String str = "";
        for (DBException ex : exceptions) {
            str += "<br>" + ex.getMessage();
        }
        return new DBExceptions(exceptions, str);
    }

    public DBExceptions(List<DBException> exceptions, String message) {
        super(message);
        this.exceptions = exceptions;
    }

    public boolean containsCode(int code) {
        for (DBException ex : exceptions) {
            if (ex.getErrorCode() == code) {
                return true;
            }
        }
        return false;
    }
}
