/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */
package ds.mpg.de.jheartdb.structures;

import java.util.ArrayList;

import org.jdom2.Element;

/**
 * Mapping from DB properties to java class.
 * 
 * @author salexan
 */
public class Info {

    private int nRecords = -1;
    private int nProperties = -1;
    private int nRecordTypes = -1;
    private int nFiles = -1;
    private String dropOffBoxPath = null;
    private ArrayList<String> flatFileList = null;

    public Info() {
    }

    public Info(final Element e) {
        setFromElement(e);
    }

    public void setFromElement(final Element e) {
        final Element e2 = e.getChild("counts");
        if (e2.getAttribute("properties") != null) {
            setnProperties(Integer.parseInt(e2.getAttributeValue("properties")));
        }
        if (e2.getAttribute("records") != null) {
            setnRecords(Integer.parseInt(e2.getAttributeValue("records")));
        }
        if (e2.getAttribute("recordTypes") != null) {
            setnRecordTypes(Integer.parseInt(e2.getAttributeValue("recordTypes")));
        }
        if (e2.getAttribute("files") != null) {
            setnFiles(Integer.parseInt(e2.getAttributeValue("files")));
        }
        if (e2.getAttribute("files") != null) {
            setnFiles(Integer.parseInt(e2.getAttributeValue("files")));
        }

        final Element e3 = e.getChild("dropOffBox");
        if (e3.getAttribute("path") != null) {
            setDropOffBoxPath(e3.getAttributeValue("path"));
        }

        flatFileList = new ArrayList<String>();
        for (final Element s : e3.getChildren()) {
            if (s.getName().equals("file")) {
                flatFileList.add(s.getAttributeValue("path"));
            }
            // else if (s.getName().equals("dir")) {
            //
            // }
        }
    }

    /**
     * @return the nRecords
     */
    public int getnRecords() {
        return nRecords;
    }

    /**
     * @param nRecords
     *            the nRecords to set
     */
    public void setnRecords(final int nRecords) {
        this.nRecords = nRecords;
    }

    /**
     * @return the nProperties
     */
    public int getnProperties() {
        return nProperties;
    }

    /**
     * @param nProperties
     *            the nProperties to set
     */
    public void setnProperties(final int nProperties) {
        this.nProperties = nProperties;
    }

    /**
     * @return the nRecordTypes
     */
    public int getnRecordTypes() {
        return nRecordTypes;
    }

    /**
     * @param nRecordTypes
     *            the nRecordTypes to set
     */
    public void setnRecordTypes(final int nRecordTypes) {
        this.nRecordTypes = nRecordTypes;
    }

    /**
     * @return the nFiles
     */
    public int getnFiles() {
        return nFiles;
    }

    /**
     * @param nFiles
     *            the nFiles to set
     */
    public void setnFiles(final int nFiles) {
        this.nFiles = nFiles;
    }

    /**
     * @return the flatFileList
     */
    public ArrayList<String> getFlatFileList() {
        return flatFileList;
    }

    /**
     * @param flatFileList
     *            the flatFileList to set
     */
    public void setFlatFileList(final ArrayList<String> flatFileList) {
        this.flatFileList = flatFileList;
    }

    /**
     * @return the dropOffBoxPath
     */
    public String getDropOffBoxPath() {
        return dropOffBoxPath;
    }

    /**
     * @param dropOffBoxPath
     *            the dropOffBoxPath to set
     */
    public void setDropOffBoxPath(final String dropOffBoxPath) {
        this.dropOffBoxPath = dropOffBoxPath;
    }
}
