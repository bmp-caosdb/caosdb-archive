/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb.dialog;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import ds.mpg.de.jheartdb.structures.Property;

/**
 * 
 * @author salexan
 */
public class PropertyTable extends JTable {

    private DefaultTableModel propertyModel = null;
    private LinkedList<Property> propertyList = null;
    // This name is a bit strange. A value of true is used for editing records
    // and
    // false is used for editing record types.
    private boolean showValueColumn = true;

    public PropertyTable(boolean showValueColumn) {
        setPropertyList(new LinkedList<Property>());
        this.showValueColumn = showValueColumn;
    }

    public final void setPropertyList(LinkedList<Property> list) {
        propertyList = list;

        if (showValueColumn) {
            // TODO: This code should be extracted to a separate class for the
            // model.
            propertyModel = new DefaultTableModel(new String[] { "Name", "Value", "Unit", "Type",
                    "Importance" }, 0) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column == 1) {
                        return true;
                    }
                    return false;
                }
            };
            setModel(propertyModel);
            for (Property p : propertyList) {
                propertyModel.addRow(new String[] { p.getName(),
                        p.getValue() == null ? "" : p.getValue().toString(),
                        p.getUnit() == null ? "" : p.getUnit(),
                        p.getType() == null ? "" : p.getType().toString(),
                        p.getImportance() == null ? "" : p.getImportance() });
            }
        } else {
            // TODO: This code should be extracted to a separate class for the
            // model.
            propertyModel = new DefaultTableModel(new String[] { "Name", "Importance" }, 0) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    if (column == 1) {
                        return true;
                    }
                    return false;
                }
            };
            setModel(propertyModel);
            TableColumn m = getColumnModel().getColumn(1);
            JComboBox c = new JComboBox();// (new String[]{"obligatory",
                                          // "recommended", "suggested"});
            c.addItem("obligatory");
            c.addItem("recommended");
            c.addItem("suggested");
            m.setCellEditor(new DefaultCellEditor(c));
            c.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            updateProperties();
                        }
                    });

                }
            });
            for (Property p : propertyList) {
                propertyModel.addRow(new Object[] { p.getName(),
                        p.getImportance() == null ? "" : p.getImportance() });
            }
        }
    }

    public void updateProperties() {
        int i = 0;
        for (Property p : propertyList) {
            p.setImportance(propertyModel.getValueAt(i, 1).toString());
            i++;
        }
    }

    public LinkedList<Property> getPropertyList() {
        for (int i = 0; i < propertyList.size(); i++) {
            Object obj = propertyModel.getValueAt(i, 1);
            String str = (String) obj;
            if (!str.equals("")) {
                propertyList.get(i).setValue(obj);
            }
        }
        return propertyList;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 0) {
            return false;
        }
        return true;
    }
}
