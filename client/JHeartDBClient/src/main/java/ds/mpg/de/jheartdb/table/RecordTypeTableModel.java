/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/*
 * Programmed at the MPIDS in Goettingen by Alexander Schlemmer.
 * 2013-01-14
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ds.mpg.de.jheartdb.table;

import ds.mpg.de.jheartdb.structures.Entity;
import ds.mpg.de.jheartdb.structures.Property;
import ds.mpg.de.jheartdb.structures.RecordType;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author salexan
 */
public class RecordTypeTableModel extends DefaultTableModel {

    private List<RecordType> records = new ArrayList<RecordType>();
    /*
     * Cache for entries:
     */
    private LinkedHashSet<String> properties = null;
    private String[] propertyNames = null;
    private int rpropLength = -1;

    public RecordTypeTableModel() {
        updateRecordCache();
    }

    public RecordTypeTableModel(List<RecordType> records) {
        this();
        setRecords(records);
    }

    public final void setRecords(List<RecordType> records) {
        this.records = records;
        updateRecordCache();
        fireTableStructureChanged();
    }

    /**
     * Updates the record cache, so that all records and their properties are
     * displayed correctly.
     */
    public void updateRecordCache() {
        properties = new LinkedHashSet<String>();
        properties.add("ID");
        properties.add("Name");
        properties.add("Description");
        updatePropertyCache();
    }

    /**
     * This method has to be called at the end of each implementation of
     * updateRecordCache().
     */
    protected void updatePropertyCache() {
        rpropLength = properties.size();
        for (RecordType r : records) {
            for (Entity p : r.getProperties()) {
                properties.add(p.getName());
            }
        }
        propertyNames = properties.toArray(new String[properties.size()]);
        setColumnIdentifiers(propertyNames);
    }

    @Override
    public Object getValueAt(int row, int column) {
        RecordType r = records.get(row);
        String pname = propertyNames[column];
        if (pname.equals("ID")) {
            return r.getId();
        } else if (pname.equals("Name")) {
            return r.getName();
        } else if (pname.equals("Description")) {
            return r.getDescription();
        } else {
            return getPropertyValueAt(row, column, pname, r);
        }
    }

    protected Object getPropertyValueAt(int row, int column, String pname, RecordType r) {
        // Properties:
        Entity p = r.getProperty(pname);
        if (p != null) {
            return p.getDescription();
        }
        return "";
    }

    @Override
    public int getColumnCount() {
        if (properties == null) {
            return 0;
        }
        return properties.size();
    }

    @Override
    public int getRowCount() {
        if (records == null) {
            return 0;
        }
        return records.size();
    }
}
